export function untargetDeadTokens() {
  game.user.targets.forEach((t) => {
    if (t.actor?.data.data.attributes.hp.value <= 0) {
      t.setTarget(false, { releaseOthers: false });
      game.user.targets.delete(t);
    }
  });
}

export function untargetAllTokens(...args) {
  const combat = args[0];
  if (game.user.isGM || canvas.tokens.controlled.find((t) => t.id === combat.previous?.tokenId)) {
    game.user.targets.forEach((t) => {
      t.setTarget(false, { releaseOthers: false });
    });
    game.user.targets.clear();
  }
}
